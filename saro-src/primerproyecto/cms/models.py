from django.db import models

# Create your models here.

class Contenido(models.Model):
    clave = models.CharField(max_length=64)
    valor = models.TextField()

    def __str__(self):
        return str(self.id) + ": "+self.clave

class Comentario(models.Model):
    contenido = models.ForeignKey(Contenido, on_delete=models.CASCADE)
    fecha = models.DateField()
    comentario = models.TextField()
    titulo = models.CharField(max_length=128)

    def __str__(self):
        return str(self.id)+ ": "+ self.titulo+"---"+ self.contenido.clave

    def validacion(self):
        return("cuerpo" in self.comentario)