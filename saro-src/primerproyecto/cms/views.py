from django.shortcuts import render

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Contenido

formulario = """
<form action="" method="POST">
    Valor: <input type="text" name="valor"
    </br> <input type=submit values="enviar">



"""

# Create your views here.
def index(request):
    return HttpResponse('Hola mundo')

@csrf_exempt
def get_resource(request, recurso):
    if request.method in ["PUT", "POST"]:
        if request.method == "POST":
            Valor = request.POST["valor"]
        else:
            Valor = request.body.decode()

        try:
            contenido = Contenido.objects.get(clave=recurso)
            contenido.valor = Valor
            contenido.save()
        except Contenido.DoesNotExist:
            contenido = Contenido(clave=recurso, valor=Valor)
            contenido.save()

    try:
        contenido = Contenido.objects.get(clave=recurso)
        respuesta = HttpResponse("El recurso pedido es: "+ contenido.clave + " Su valor es: "+ contenido.valor + " su identificador es: "
                                 + str(contenido.id))
    except Contenido.DoesNotExist:
        return HttpResponse(formulario)
    return respuesta





